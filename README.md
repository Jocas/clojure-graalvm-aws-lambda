# clojure-graalvm-aws-lambda

Custom AWS Lambda runtime demo project with Clojure CLI GraalVM and deps.edn.

## What is in the package

Most common things are scripted in the Makefile:
- `make build-native-image`: inside the docker with the GraalVM builds native binary for the lambda and copies it to the project folder two files: `server` and `libsunec.so`
- `make build-lambda-zip`: inside the docker creates a custom AWS lambda runtime zip archive and copies it to the project folder as a `function.zip` file.
- `make deploy-custom-runtime-lambda`: (use it if you have `awscli` installed) builds a deployable lambda zip and deploys it to AWS.
- `make deploy-lambda-via-container`: builds lambda zip and deploys to AWS with your provided credentials (replace parameters inside the make file).

## CI

`.gitlab-ci.yml` file includes a demo on how the lambda can be deployed from the Gitlab CI pipeline. Create environment variables in the CI/CD setup of your project.

## Prerequisites

This demo assumes that docker is available.

## Usage

- Clone the repo
- Configure CI
- Add your Clojure project deps (with a little more work Git deps from private git repositories can be achieved).
- Modify `request->response` function
- Compile to native image with GraalVM inside a docker
- Deploy to AWS Lambda

```clojure
(defn request->response [request]
  (let [decoded-request (json/read-value request read-mapper)]
    (json/write-value-as-string decoded-request)))
```
